import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


/**
 * Home component
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  constructor(private readonly router: Router) { }

  /**
   * Handles button click event to cake builder
   */
  public onClick(): void {
    this.router.navigate([`/builder/${3031}`]);
    // { queryParams: { id: 3031 } });
  }
}
