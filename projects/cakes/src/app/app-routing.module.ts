import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/components/home.component';

const ROUTES: Routes = [
  { path: 'order', loadChildren: () => import('./order/order.module').then(m =>  m.OrderModule) },
  { path: 'builder', loadChildren: () => import('./builder/builder.module').then(m => m.BuilderModule) },
  { path: '', component: HomeComponent, pathMatch: 'full' }
];

/**
 * App routing module
 */
@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
