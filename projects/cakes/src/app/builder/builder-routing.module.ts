import { Injectable, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppOverlayService } from 'projects/lib/src/lib/app-overlay/services/app-overlay.service';
import { Product } from 'projects/lib/src/lib/products/models/product.model';
import { ProductService } from 'projects/lib/src/lib/products/services/product.service';
import { BaseDataResolver } from 'projects/lib/src/public-api';
import { BuilderComponent } from './components/builder/builder.component';

/**
 * Product builder object resolver
 */
@Injectable()
export class BuilderResolver extends BaseDataResolver<Product> {
  constructor(
    service: ProductService,
    overlayService: AppOverlayService) {
    super(service, overlayService);
  }
}

const routes: Routes = [
  { path: ':id', component: BuilderComponent, resolve: { value: BuilderResolver }, },
];

/**
 * Builder routing module
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [BuilderResolver]
})
export class BuilderRoutingModule { }
