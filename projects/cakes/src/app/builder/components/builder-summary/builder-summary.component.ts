import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

/**
 * Represents the Summary Box
 */
@Component({
  selector: 'app-builder-summary',
  templateUrl: './builder-summary.component.html',
  styleUrls: ['./builder-summary.component.scss']
})
export class BuilderSummaryComponent {

  constructor(private readonly router: Router) { }

  public showCheckoutButton: boolean = true;

  /**
   * apply sticky to builder summary box
   */
  public sticky: boolean = false;

  /**
   * Current position of the summary box
   */
  public currentPosition: number;

  @ViewChild('summaryBox') summaryBox: ElementRef;

  /** {@inheritdoc} */
  public ngAfterViewInit(): void {
    this.currentPosition = this.summaryBox.nativeElement.offsetTop;
  }

  @HostListener('window:scroll', ['$event'])
  public handleScroll() {
    const windowScroll = window.pageYOffset;
    this.sticky = windowScroll >= this.currentPosition;
  }

  /**
   * Handles click event to checkout
   */
  public onClick(): void {
    this.router.navigate([`/order/review`]);
  }
}
