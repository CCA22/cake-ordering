import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuilderSummaryComponent } from './builder-summary.component';

describe('BuilderSummaryComponent', () => {
  let component: BuilderSummaryComponent;
  let fixture: ComponentFixture<BuilderSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuilderSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuilderSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
