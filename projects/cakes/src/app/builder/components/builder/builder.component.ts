import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

/**
 * builder component
 */
@Component({
  selector: 'app-builder',
  templateUrl: './builder.component.html',
  styleUrls: ['./builder.component.scss']
})
export class BuilderComponent implements OnInit {

  /**
   * Our value
   */
  // tslint:disable-next-line: no-any
  public value: any;


  private readonly subscriptions: Subscription[] = [];

  constructor(route: ActivatedRoute) {
    this.subscriptions.push(route.data.subscribe(data => {
      this.value = data.value;
    }));
  }

  /** {@inheritdoc} */
  public ngOnInit(): void {
  }

}
