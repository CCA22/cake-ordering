import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { GuestCheckoutModule } from 'projects/lib/src/public-api';
import { BuilderRoutingModule } from './builder-routing.module';
import { BuilderComponent } from './components/builder/builder.component';
import { BuilderSummaryComponent } from './components/builder-summary/builder-summary.component';


/**
 * Order routing module
 */
@NgModule({
  declarations: [
    BuilderComponent,
    BuilderSummaryComponent,
  ],
  imports: [
    BuilderRoutingModule,
    CommonModule,
    GuestCheckoutModule,
  ],
})
export class BuilderModule { }
