import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HeroComponent } from './components/hero.component';

/**
 * Hero module
 */
@NgModule({
  declarations: [HeroComponent],
  imports: [
    CommonModule
  ]
})
export class HeroModule { }
