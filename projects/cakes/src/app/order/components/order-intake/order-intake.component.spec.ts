import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderIntakeComponent } from './order-intake.component';

describe('OrderIntakeComponent', () => {
  let component: OrderIntakeComponent;
  let fixture: ComponentFixture<OrderIntakeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderIntakeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderIntakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
