import { Component, OnInit } from '@angular/core';

/**
 * Review Order
 */
@Component({
  selector: 'app-order-review',
  templateUrl: './order-review.component.html',
  styleUrls: ['./order-review.component.scss']
})
export class OrderReviewComponent implements OnInit {

  /** {@inheritdoc} */
  public ngOnInit(): void {
  }

}
