import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/components/home.component';
import { OrderIntakeComponent } from './components/order-intake/order-intake.component';
import { OrderReviewComponent } from './components/order-review/order-review.component';

const routes: Routes = [
  { path: 'intake', component: OrderIntakeComponent },
  { path: 'review', component: OrderReviewComponent },
];

/**
 * Order routing module
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class OrderRoutingModule { }
