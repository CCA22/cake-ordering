import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormGroup, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IconsModule } from 'lib';
import { GuestCheckoutModule } from 'projects/lib/src/public-api';
import { BuilderSummaryComponent } from '../builder/components/builder-summary/builder-summary.component';
import { OrderIntakeComponent } from './components/order-intake/order-intake.component';
import { OrderReviewComponent } from './components/order-review/order-review.component';
import { OrderComponent } from './components/order.component';
import { OrderRoutingModule } from './order-routing.module';


/**
 * Order module
 */
@NgModule({
  declarations: [
    OrderComponent,
    OrderReviewComponent,
    OrderIntakeComponent,
  ],
  imports: [
    CommonModule,
    OrderRoutingModule,
    IconsModule,
    FormsModule,
    GuestCheckoutModule,
    NgbModule,
  ]
})
export class OrderModule { }
