import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalEngineModule } from 'projects/lib/src/lib/modal-engine/modal-engine.module';
import { OffCanvasModule } from 'projects/lib/src/lib/off-canvas/off-canvas.module';
import { ToastsModule } from 'projects/lib/src/lib/toast/toasts.module';
import { LibModule } from 'projects/lib/src/lib/lib.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BuilderModule } from './builder/builder.module';
import { HeroModule } from './hero/hero.module';
import { HomeComponent } from './home/components/home.component';
import { OrderModule } from './order/order.module';
import { FooterComponent } from './footer/components/footer.component';

/**
 * App main module
 */
@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    LibModule,
    ModalEngineModule,
    FormsModule,
    ToastsModule,
    OffCanvasModule,
    HeroModule,
    OrderModule,
    BuilderModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
