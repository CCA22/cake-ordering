import { ChangeDetectorRef, Component, OnInit } from '@angular/core';

import { AppComponentBase } from 'projects/lib/src/public-api';

const APP_TITLE = 'Cake Ordering';

/**
 * Main app component
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends AppComponentBase {
  /** {@inheritdoc} */
  protected readonly appTitle = APP_TITLE;
}
