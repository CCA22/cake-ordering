/*
 * Public API Surface of lib
 */

import '@angular/localize/init'; // WORKAROUND: we use $localize in the lib; not added automatically by "ng add" (https://github.com/just-jeb/angular-builders/issues/678)

export * from './lib/app-component-base';
export * from './lib/app-overlay/enums/app-overlays.enum';
export * from './lib/app-overlay/services/app-overlay.service';
export * from './lib/auth/auth.module';
export * from './lib/auth/services/auth.service';
export * from './lib/browser-title/browser-title.service';
export * from './lib/busy-indicator/busy-indicator.component';
export * from './lib/busy-indicator/busy-indicator.module';
export * from './lib/icons/icons.module';
export * from './lib/lib.module';
export * from './lib/modal-engine/modal-engine.module';
export * from './lib/modal-engine/services/modal-engine.service';

export * from './lib/guest-checkout/components/guest-checkout.component';
export * from './lib/guest-checkout/guest-checkout.module';

export * from './lib/orders/services/order.service';
export * from './lib/orders/models/order.model';
export * from './lib/orders/models/order-intake.model';
export * from './lib/orders/order-summary.module';
export * from './lib/products/services/product.service';
export * from './lib/shared/base-data.resolver';
export * from './lib/shared/enums/contextual-color.enum';
export * from './lib/shared/guards/location-selector.guard';
export * from './lib/shared/services/location.service';
export * from './lib/shared/services/persistence.service';
export * from './lib/shared/services/string.service';
export * from './lib/shared/services/url.service';
export * from './lib/shared/shared.module';
export * from './lib/toast/components/toasts/toasts.component';
export * from './lib/toast/services/toast.service';
export * from './lib/toast/toasts.module';
export * from './lib/users/users.module';
export * from './lib/users/models/user.model';
