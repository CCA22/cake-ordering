import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentsComponent } from './components/components.component';


/**
 * Module for Location Selector
 */
@NgModule({
  declarations: [
    ComponentsComponent,
  ],
  imports: [
    CommonModule,
  ]
})
export class LocationSelectorModule { }
