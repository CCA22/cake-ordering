import { CommonModule } from '@angular/common';

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AuthModule } from './auth/auth.module';
import { BusyIndicatorComponent } from './busy-indicator/busy-indicator.component';
import { BusyIndicatorModule } from './busy-indicator/busy-indicator.module';
import { IconsModule } from './icons/icons.module';
import { ModalEngineModule } from './modal-engine/modal-engine.module';
import { ToastsModule } from './toast/toasts.module';
import { OffCanvasModule } from './off-canvas/off-canvas.module';
import { GuestCheckoutModule } from './guest-checkout/guest-checkout.module';


/**
 * Our main module
 */
@NgModule({
  imports: [

    IconsModule,
    CommonModule,
    BrowserModule,
    ModalEngineModule,
    ToastsModule,
    BusyIndicatorModule,
    AuthModule,
    OffCanvasModule,
    GuestCheckoutModule,
  ],
  exports: [
    IconsModule,
    BusyIndicatorComponent,
    ToastsModule,
    GuestCheckoutModule,
  ],
})
export class LibModule { }
