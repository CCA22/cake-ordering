import { TestBed } from '@angular/core/testing';
import { tap } from 'rxjs/operators';
import { AppOverlays } from '../enums/app-overlays.enum';
import { AppOverlayService } from './app-overlay.service';

describe('AppOverlayService', () => {
  let overlayService: AppOverlayService;

  beforeEach(() => {
    TestBed.configureTestingModule({ });

    overlayService = TestBed.inject(AppOverlayService);
  });

  it('should be created', () => {
    expect(overlayService).toBeTruthy();
  });

  describe('showOverlay()', () => {
    it('should add overlay', (done) => {
      let count = 0;
      overlayService.overlays$.pipe(
        tap (() => count++)
      ).subscribe(request => {
        const overlays = request.overlay;
        expect(overlays & AppOverlays.ApplicationError).not.toEqual(0);
        if (count === 1) {
          expect(overlays & AppOverlays.Loading).toEqual(0);
        } else {
          expect(overlays & AppOverlays.Loading).not.toEqual(0);
        }
        done();
      });

      overlayService.showOverlay(AppOverlays.ApplicationError);
      overlayService.showOverlay(AppOverlays.Loading);
    });
  });

  describe('hideOverlay()', () => {
    it('should remove from overlays', (done) => {
      let count = 0;
      overlayService.overlays$.pipe(
        tap (() => count++)
      ).subscribe(request => {
        const overlays = request.overlay;
        if (count === 1) {
          expect(overlays & AppOverlays.ApplicationError).not.toEqual(0);
        } else {
          expect(overlays).toEqual(0);
          done();
        }
      });

      overlayService.showOverlay(AppOverlays.ApplicationError);
      overlayService.hideOverlay(AppOverlays.ApplicationError);
    });

    it('should not remove multirequest overlay until all have hidden', (done) => {
      let count = 0;
      overlayService.overlays$.pipe(
        tap (() => count++)
      ).subscribe(request => {
        expect(count < 3); // should only fire once for show and once for hide
        const overlays = request.overlay;
        if (count === 1) {
          expect(overlays & AppOverlays.Loading).not.toEqual(0);
        } else {
          expect(overlays).toEqual(0);
          done();
        }
      });

      overlayService.showOverlay(AppOverlays.Loading);
      overlayService.showOverlay(AppOverlays.Loading);

      overlayService.hideOverlay(AppOverlays.Loading);
      overlayService.hideOverlay(AppOverlays.Loading);
    });

    it('should pass parameter through overlay request', (done) => {
      const testParam = 'testParam';
      overlayService.overlays$
        .subscribe(request => {
          expect(request.params).toEqual(testParam);
          done();
        });

        overlayService.showOverlay(AppOverlays.Loading, testParam);
    });
  });
});
