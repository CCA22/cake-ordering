import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { OVERLAY_CONFIGURATION } from '../constants/overlay-configuration';
import { AppOverlays } from '../enums/app-overlays.enum';
import { AppOverlayConfig } from '../models/app-overlay-config.model';
import { AppOverlayRequest } from '../models/app-overlay-request.model';

/**
 * Application overlay management
 */
@Injectable({
  providedIn: 'root'
})
export class AppOverlayService {
  /**
   * The current overlays to display
   */
  public overlays$: Observable<AppOverlayRequest>;

  private readonly configurations: AppOverlayConfig[];
  private readonly multiRequestState: { [x: string]: number; } = { };
  private readonly overlays = new Subject<AppOverlayRequest>();
  private currentOverlays: AppOverlays;

  constructor() {
    this.overlays$ = this.overlays.asObservable();
    this.currentOverlays = AppOverlays.None;
    this.configurations = OVERLAY_CONFIGURATION;
    this.initializeMultiRequestState();
  };

  /**
   * Requests an overlay be displayed
   * @param overlay The overlay to display
   * @param params Parameters to pass through the request
   */
  // tslint:disable-next-line: no-any
  public showOverlay(overlay: AppOverlays, params: any = null): void {
    const configuration = this.configurations.find(c => c.type === overlay);
    if (configuration) {
      // increment request counts for multis
      if (configuration.multiRequest) {
        this.multiRequestState[configuration.type]++;
      }

      this.addOverlay(overlay, params);
    }
  }

  /**
   * Hide an overlay
   * @param overlay The overlay to hide
   */
  public hideOverlay(overlay: AppOverlays): void {
    const configuration = this.configurations.find(c => c.type === overlay);
    if (configuration) {
      let newState = 0;
      if (configuration.multiRequest) {
        newState = Math.max(--this.multiRequestState[configuration.type], 0);
        this.multiRequestState[configuration.type] = newState;
      }

      // State will only be > 0 for multis that have had multiple requests
      //  in which case we skip hiding it until their state is 0,
      //  (everyone that has shown it has hidden it)
      if (newState === 0) {
        this.removeOverlay(overlay);
      }
    }
  }
  /**
   * Adds multirequest overlays to state tracking
   */
  private initializeMultiRequestState(): void {
    this.configurations.forEach(configuration => {
      if (configuration.multiRequest) {
        this.multiRequestState[configuration.type] = 0;
      }
    });
  }

  /**
   * Adds an overlay to current overlays if it doesn't exist
   * @param overlay The overlay to add
   * @param params Parameters to pass through the request
   */
  // tslint:disable-next-line: no-any
  private addOverlay(overlay: AppOverlays, params: any): void {
    if (((overlay as number) & (this.currentOverlays as number)) === 0) {
      this.currentOverlays |= overlay;
      this.overlays.next({
        overlay: this.currentOverlays,
        params
      });
    }
  }

  /**
   * Removes an overlay from current overlays if it exists
   * @param overlay The overlay to remove
   */
  private removeOverlay(overlay: AppOverlays): void {
    if (((overlay as number) & (this.currentOverlays as number)) !== 0) {
      this.currentOverlays &= ~overlay;
      this.overlays.next({
        overlay: this.currentOverlays,
        params: null
      });
    }
  }
}
