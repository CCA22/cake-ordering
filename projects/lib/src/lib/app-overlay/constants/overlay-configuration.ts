import { AppOverlays } from '../enums/app-overlays.enum';
import { AppOverlayConfig } from '../models/app-overlay-config.model';

export const OVERLAY_CONFIGURATION: AppOverlayConfig[] = [
  {
    type: AppOverlays.ApplicationError
  },
  {
    type: AppOverlays.Loading,
    multiRequest: true
  }
];
