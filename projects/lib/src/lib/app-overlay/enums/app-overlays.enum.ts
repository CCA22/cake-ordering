/**
 * Enumeration for the various types of overlays
 */
export enum AppOverlays {

  /** Convenience non-value */
  None = 0,

  /** Application error overlay */
  ApplicationError = 1,

  /** Application Loading overlay */
  Loading = 2,

}
