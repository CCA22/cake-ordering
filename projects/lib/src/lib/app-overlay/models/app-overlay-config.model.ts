import { AppOverlays } from '../enums/app-overlays.enum';

/**
 * Overlay configuration object
 */
export interface AppOverlayConfig {
  /**
   * The overlay type
   */
  type: AppOverlays;

  /**
   * Whether or not the overlay supports multiple stacking requests
   */
  multiRequest?: boolean;
}
