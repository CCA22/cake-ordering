import { AppOverlays } from '../enums/app-overlays.enum';

/**
 * Represents an application overlay request sent to subscribers
 */
export interface AppOverlayRequest {
  /**
   * The parameters of the request
   */
  // tslint:disable-next-line: no-any
  params: any;

  /**
   * The overlay to display
   */
  overlay: AppOverlays;
}