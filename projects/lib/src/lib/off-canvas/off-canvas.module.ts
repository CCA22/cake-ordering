import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IconsModule } from '../icons/icons.module';
import { OffCanvasComponent } from './off-canvas.component';

/**
 * Off-Canvas components
 */
@NgModule({
  declarations: [
    OffCanvasComponent
  ],
  imports: [
    CommonModule,
    IconsModule,
  ],
  exports: [
    OffCanvasComponent
  ],
})
export class OffCanvasModule { }
