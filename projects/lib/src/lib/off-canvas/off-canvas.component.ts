import { Component, EventEmitter, Input, Output  } from '@angular/core';

/**
 * Off-Canvas container component
 */
@Component({
  selector: 'lib-off-canvas',
  templateUrl: './off-canvas.component.html',
  styleUrls: ['./off-canvas.component.scss']
})
export class OffCanvasComponent {

  /**
   * Fires when the collapsed state changes
   *
   * @param param true if collapsed; false if expanded
   */
  @Output() public collapsedChanged = new EventEmitter<boolean>();

  private isCollapsed = true;

  /**
   * Get indicator for current collapsed state
   */
  public get collapsed(): boolean {
    return this.isCollapsed;
  }

  /**
   * Set the current collapsed state
   *
   * @param value true for collapsed; false for expanded
   */
  @Input() public set collapsed(value: boolean) {
    this.isCollapsed = value;
  }

  /**
   * Toggle the collapsed state
   */
  public toggleCollapsed(): void {
    this.isCollapsed = !this.isCollapsed;
    this.collapsedChanged.emit(this.isCollapsed);
  }
}
