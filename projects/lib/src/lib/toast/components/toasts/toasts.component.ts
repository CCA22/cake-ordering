import { Component, TemplateRef } from '@angular/core';
import { Toast } from '../../models/toast.model';
import { ToastService } from '../../services/toast.service';

/**
 * Toast notification component
 */
@Component({
  selector: 'lib-toasts',
  templateUrl: './toasts.component.html',
  styleUrls: ['./toasts.component.scss']
})
export class ToastsComponent {

  constructor(private readonly toastService: ToastService) { }

  /**
   * Checks if toast contains body template
   * @param toast toast notification data
   * @returns boolean value of the result
   */
  public isBodyTemplate(toast: Toast): boolean {
    return toast.body instanceof TemplateRef;
  }

  /**
   * Removes toast from the list
   * @param toast toast notification data
   */
  public remove(toast: Toast): void {
    this.toastService.remove(toast);
  }

  /**
   * Function to get current toasts array
   * @returns toasts array
   */
  public getToasts(): Toast[] {
    return this.toastService.toasts;
  }

}
