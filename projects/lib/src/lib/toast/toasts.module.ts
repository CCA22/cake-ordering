import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IconsModule } from '../icons/icons.module';
import { ToastsComponent } from './components/toasts/toasts.component';

/**
 * Dynamic toast popup
 */
@NgModule({
  declarations: [ToastsComponent],
  imports: [
    CommonModule,
    NgbModule,
    IconsModule
  ],
  exports: [
    ToastsComponent
  ]
})
export class ToastsModule { }
