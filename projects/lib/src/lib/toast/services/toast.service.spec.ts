import { TestBed } from '@angular/core/testing';
import { ToastService } from './toast.service';
import { Toast } from '../models/toast.model';

describe('ToastsService', () => {
  let service: ToastService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ToastService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add toast to the array', () => {
    const toast: Toast = { body: 'This is a toast without styles!' };
    service.show(toast);
    expect(service.toasts[0]).toEqual(toast);
  });

  it('should remove toast from the array', () => {
    const toast: Toast = { body: 'This is a toast without styles!' };
    service.show(toast);
    service.remove(toast);
    expect(service.toasts.length).toEqual(0);
  });

  it('should not throw errors for non-existing toast', () => {
    const toast: Toast = { body: 'This is a toast without styles!' };
    service.remove(toast);
    expect(service.toasts.length).toEqual(0);
  });

});
