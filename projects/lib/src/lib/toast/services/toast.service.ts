import { Injectable } from '@angular/core';
import { Toast } from '../models/toast.model';

const DEFAULT_DELAY = 4000;

/**
 * Toast notification service
 */
@Injectable({ providedIn: 'root' })
export class ToastService {

  /**
   * Toast notification service
   */
  public toasts: Toast[] = [];

  /**
   * General function to show toast notification
   * @param toast toast notification data
   */
  public show(toast: Toast): void {
    toast.autohide = toast.autohide ?? true;
    this.toasts.push(toast);

    if (!!toast.autohide) {
      setTimeout(() => this.remove(toast), toast.delay || DEFAULT_DELAY);
    }
  }

  /**
   * General function to remove toast notification
   * @param toast toast notification data
   */
  public remove(toast: Toast): void {
    this.toasts = this.toasts.filter(t => t !== toast);
  }
}
