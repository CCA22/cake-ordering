import { TemplateRef } from '@angular/core';
import { ContextualColor } from '../../shared/enums/contextual-color.enum';

/**
 * Toast data model
 */
export interface Toast {

  /**
   * Popup delay
   */
  delay?: number;

  /**
   * Autohide parameter
   */
  autohide?: boolean;

  /**
   * Alert style type
   */
  type?: ContextualColor;

  /**
   * Content text
   */
  // tslint:disable-next-line: no-any
  body: string | TemplateRef<any>;

  /**
   * Toast title (optional)
   */
  title?: string;
  /**
   * Toast icon (optional)
   */
  icon?: string;

}
