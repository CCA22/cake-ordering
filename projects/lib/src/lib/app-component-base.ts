import { Directive, OnInit, ChangeDetectorRef } from '@angular/core';
import { AppOverlayService } from './app-overlay/services/app-overlay.service';
import { AppOverlays } from './app-overlay/enums/app-overlays.enum';
import { BrowserTitleService } from './browser-title/browser-title.service';
import { forkJoin } from 'rxjs';
import { ProductService } from 'lib';

/**
 * Base class for root app components
 */
@Directive()  // not really a directive, but required by Ivy to allow this class to be used as a base class for components
// tslint:disable-next-line: directive-class-suffix
export abstract class AppComponentBase implements OnInit {
  /**
   * Indicates when the application content has been loaded and is ready for display
   */
  public isContentReady = false;

  /**
   * Indicates if the application is current loading
   */
  public isLoading = true;

  /**
   * Application title string
   */
  protected abstract readonly appTitle: string;

  /**
   * Application does not require authenticated user -- must be set before ngOnInit()
   */
  protected allowAnonymous = false;

  constructor(
    private readonly overlayService: AppOverlayService,
    private readonly browserTitleService: BrowserTitleService,
    private readonly productService: ProductService,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) { }

  /** {@inheritdoc} */
  public ngOnInit(): void {
    this.overlayService.overlays$.subscribe(request => {
      const overlay = request.overlay;
      const params = request.params;
      this.isLoading = ((overlay as number) & (AppOverlays.Loading)) !== 0;
      this.changeDetectorRef.detectChanges();
      console.log('...');
    });
    this.overlayService.showOverlay(AppOverlays.Loading);
    forkJoin([
      this.productService.get('3031')
    ]).subscribe(
      results => {
        this.browserTitleService.appTitle = this.appTitle;
        this.startupSuccessful();
      }
    );
  }
  private startupSuccessful(): void {
    this.isContentReady = true;
    this.overlayService.hideOverlay(AppOverlays.Loading);
  }
}
