import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseDataService } from '../../shared/services/base-data.service';
import { UrlService } from '../../shared/services/url.service';
import { Product } from '../models/product.model';

const basePath = '/products';

/**
 * Product management
 */
@Injectable({
  providedIn: 'root'
})
export class ProductService extends BaseDataService<Product, Product, Product> {

  constructor(urlService: UrlService, httpClient: HttpClient) {
    super(basePath, urlService, httpClient);
  }

}
