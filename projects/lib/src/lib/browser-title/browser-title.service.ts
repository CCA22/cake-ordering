import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

/**
 * Browser title change service
 */
@Injectable({
  providedIn: 'root'
})
export class BrowserTitleService {

  /**
   * App title for appending to the browser title
   */
  public appTitle: string;

  constructor(private readonly titleService: Title) { }

  /**
   * Set browser title
   *
   * @param title current page title
   */
  public setTitle(title: string): void {
    this.titleService.setTitle(`${title} - ${this.appTitle}`);
  }

  /**
   * Get current browser title
   */
  public getTitle(): string {
    return this.titleService.getTitle();
  }

}
