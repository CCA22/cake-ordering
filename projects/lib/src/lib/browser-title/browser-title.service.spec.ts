import { TestBed } from '@angular/core/testing';

import { BrowserTitleService } from './browser-title.service';

describe('BrowserTitleService', () => {
  let service: BrowserTitleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BrowserTitleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
