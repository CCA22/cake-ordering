import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from 'projects/lib/src/public-api';
import { ModalEngineService } from '../../modal-engine/services/modal-engine.service';

/**
 *
 */
@Component({
  selector: 'lib-guest-checkout',
  templateUrl: './guest-checkout.component.html',
  styleUrls: ['./guest-checkout.component.scss']
})
export class GuestCheckoutComponent {

  /** User */
  public user: User;

  /**
   * Dirty status for the currently selected preset
   */
  public isDirty = false;

  /**
   * Key for peristing data
   */
  @Input() public persistenceKey: string;

  @Input() public shouldOpen = true;

  // tslint:disable-next-line: no-any
  @ViewChild('guestCheckoutModal') private readonly guestCheckoutModal: TemplateRef<any>;
  @ViewChild('guestCheckoutForm') private readonly guestCheckoutForm: NgForm;

  constructor(
    private readonly modalService: ModalEngineService,
  ) { }

  /** {@inheritdoc} */
  public ngOnInit(): void {
    this.showGuestAccountPrompt();
    // Promise.resolve(null).then(() => {
    //     this.showGuestAccountPrompt();
    // });
    // this.showGuestAccountPrompt
  }

  private showGuestAccountPrompt(): void {
    const buttons = [{ buttonClass: 'btn btn-primary', buttonIcon: '', buttonText: 'Continue', id: 'save' },];
    this.modalService.customDialog('Guest Checkout',
      this.guestCheckoutModal,
      buttons,
      button => {
        switch (button.id) {
          case 'save':
            this.guestCheckoutForm.form.markAllAsTouched();
            console.log(`click ${button.id}`);
            break;
        }
      }, null, { backdrop: 'static' });
  }
}
