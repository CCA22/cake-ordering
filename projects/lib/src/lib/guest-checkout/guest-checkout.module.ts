import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconsModule } from '../icons/icons.module';
import { FormsModule } from '@angular/forms';
import { GuestCheckoutComponent } from './components/guest-checkout.component';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalEngineModule } from '../modal-engine/modal-engine.module';

/**
 *
 */
@NgModule({
  declarations: [
    GuestCheckoutComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    GuestCheckoutComponent
  ]
})
export class GuestCheckoutModule { }
