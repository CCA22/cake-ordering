/**
 * OrderIntake
 */
export interface OrderIntake {
  /**
   * Pickup date
   */
  Date: string;

  /**
   * Pickup time
   */
  Time: string;

  /**
   * Pickup Location
   */
  Location: string;
}
