import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseDataService } from '../../shared/services/base-data.service';
import { UrlService } from '../../shared/services/url.service';
import { Order } from '../models/order.model';

const basePath = '/orders';

/**
 * Order management
 */
@Injectable({
  providedIn: 'root'
})
export class OrderService extends BaseDataService<Order, Order, Order> {

  constructor(urlService: UrlService, httpClient: HttpClient) {
    super(basePath, urlService, httpClient);
  }
}
