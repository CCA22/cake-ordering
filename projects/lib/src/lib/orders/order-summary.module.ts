import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderSummaryComponent } from './components/order-summary/order-summary.component';

/**
 * Order Module
 */
@NgModule({
  declarations: [OrderSummaryComponent],
  imports: [
    CommonModule
  ]
})
export class OrderSummaryModule { }
