import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { IconsModule } from '../icons/icons.module';
import { ModalViewComponent } from './components/modal-view/modal-view.component';

/**
 * Dynamic modal popup
 */
@NgModule({
  declarations: [
    ModalViewComponent,
  ],
  entryComponents: [
    ModalViewComponent,
  ],
  imports: [
    CommonModule,
    NgbModule,
    IconsModule,
  ],
  exports: []
})
export class ModalEngineModule { }
