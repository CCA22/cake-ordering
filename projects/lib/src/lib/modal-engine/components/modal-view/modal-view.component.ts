import { Component, Input, TemplateRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalButton } from '../../models/modal-button.model';

/**
 * Modal window component
 */
@Component({
  selector: 'lib-modal-view',
  templateUrl: './modal-view.component.html',
  styleUrls: ['./modal-view.component.scss']
})
export class ModalViewComponent {

  /**
   * Title string
   */
  @Input() public title: string;

  /**
   * String or template content
   */
  // tslint:disable-next-line: no-any
  @Input() public content: string | TemplateRef<any>;

  /**
   * Actions buttons
   */
  @Input() public buttons: ModalButton[];

  /**
   * Fired when the user clicks an action button
   */
  @Input() public actionClick: (button: ModalButton, data?: string) => void;

  /**
   * Fired when the user clicks close button
   */
  @Input() public closeClick: () => void;

  constructor(public readonly activeModal: NgbActiveModal) { }

  /**
   * Inidicates if the content is a template
   */
  public get isContentTemplate(): boolean {
    return this.content instanceof TemplateRef;
  }

  /**
   * Action button click handler
   * @param button clicked button data
   */
  public onClick(button: ModalButton): void {
    this.actionClick(button);
    if (button.autoClose ?? true) {
      this.activeModal.close(button);
    }
  }

  /**
   * Handler for dialog closure
   */
  public onCloseClick(): void {
    if (!!this.closeClick) {
      this.closeClick();
    }
    this.activeModal.dismiss();
  }
}
