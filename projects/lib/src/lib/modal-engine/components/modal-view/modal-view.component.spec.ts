import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { ModalViewComponent } from './modal-view.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalButton } from '../../models/modal-button.model';

describe('ModalViewComponent', () => {
  let component: ModalViewComponent;
  let fixture: ComponentFixture<ModalViewComponent>;

  const testTitle: string = "Sample Title";
  const testContent: string = "Sample Title";
  const testButtons: ModalButton[] = [
    { buttonClass: 'btn-primary', buttonIcon: 'check', id: 'confirm' },
    { buttonClass: 'btn-primary', buttonText: `Cancel`, id: 'confirm' }
  ];
  const testCallback = (_button: ModalButton, _data: any) => {
    // console.log(button, data);
  }

  function getButtons() {
    return fixture.debugElement.queryAll(By.css('.main-buttons'));
  }

  function getIcons() {
    return fixture.debugElement.queryAll(By.css('.icon'));
  }

  function getSpans() {
    return fixture.debugElement.queryAll(By.css('.main-buttons span'));
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalViewComponent],
      providers: [NgbActiveModal]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalViewComponent);
    component = fixture.componentInstance;
    component.title = testTitle;
    component.content = testContent;
    component.buttons = testButtons;
    component.actionClick = testCallback;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set correct title', () => {
    const title = fixture.debugElement.query(By.css('.modal-title'));

    expect(title).toBeTruthy();
    expect(title.nativeElement.textContent).toEqual(testTitle);
  });

  it('should set correct content', () => {
    const content = fixture.debugElement.query(By.css('.modal-body'));

    expect(content).toBeTruthy();
    expect(content.nativeElement.textContent).toEqual(testContent);
  });

  it('should set correct buttons', () => {
    const buttons = getButtons();
    const icons = getIcons();
    const spans = getSpans();

    const testIcons = testButtons.filter(button => button.buttonIcon);
    const testSpans = testButtons.filter(button => button.buttonText);

    expect(buttons).toBeTruthy();
    expect(buttons.length).toEqual(testButtons.length);
    expect(icons.length).toEqual(testIcons.length);
    expect(spans.length).toEqual(testSpans.length);

    for (let i = 0; i < testSpans.length; i++) {
      expect(spans[i].nativeElement.textContent).toEqual(testSpans[i].buttonText);
    }
  });

  it('should execute callback', fakeAsync(() => {
    spyOn(component, 'actionClick');

    const buttons = getButtons();

    for (let i = 0; i < testButtons.length; i++) {
      buttons[i].nativeElement.click();
      tick();

      expect(component.actionClick).toHaveBeenCalledWith(testButtons[i]);
    }

    expect(component.actionClick).toHaveBeenCalledTimes(testButtons.length);
  }));

});
