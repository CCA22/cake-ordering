/**
 * Popup button data
 */
export interface ModalButton {

  /**
   * User-defined unique identifier
   */
  id: string | number;

  /**
   * Indicates if dialog should stay opened
   */
  autoClose?: boolean;

  /**
   * Data supplied with the button
   */
  data?: string | number | boolean;

  /**
   * Font Awesome icon
   */
  buttonIcon?: string | string[];

  /**
   * Icon class name(s) to apply, if any
   */
  iconClass?: string;

  /**
   * Popup button text
   */
  buttonText?: string;

  /**
   * Applied classes to the popup button
   */
  buttonClass?: string;

}
