import { TestBed } from '@angular/core/testing';

import { ModalEngineService } from './modal-engine.service';
import { ModalButton } from '../models/modal-button.model';
import { ModalViewComponent } from '../components/modal-view/modal-view.component';
import { NgbModal, NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('ModalEngineService', () => {
  let service: ModalEngineService;
  let ngbModal: NgbModal;

  let spyDialogRef: any = {
    componentInstance: {}
  };

  const testTitle: string = "Sample Title";
  const testButtons: ModalButton[] = [
    { buttonClass: 'btn-primary', buttonIcon: 'check', id: 'confirm' },
    { buttonClass: 'btn-primary', buttonText: `Cancel`, id: 'confirm' }
  ];
  const testCallback = (_button: ModalButton, _data: any): void => {
    // console.log(button, data);
  }
  const testParams = { size: 'lg', backdrop: 'static' };
  const deleteTestParams = { backdrop: 'static' };
  const emptyTestParams = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbModule
      ],
      providers: [
        NgbActiveModal,
        ModalViewComponent
      ]
    }).compileComponents();

    service = TestBed.inject(ModalEngineService);
    ngbModal = TestBed.get(NgbModal);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('customDialog()', () => {
    it('should open modal', () => {
      const dialogSpy = spyOn(ngbModal, 'open').and.returnValue(spyDialogRef);

      service.customDialog(testTitle, testTitle, testButtons, testCallback);
      expect(dialogSpy).toHaveBeenCalledTimes(1);
      expect(dialogSpy).toHaveBeenCalledWith(ModalViewComponent, emptyTestParams);
    });
  });

  describe('alertDialog()', () => {
    let dialogSpy;

    beforeEach(() => {
      dialogSpy = spyOn(ngbModal, 'open').and.returnValue(spyDialogRef);
      service.alertDialog(testTitle, testTitle, testCallback);
    });

    it('should open modal', () => {
      expect(dialogSpy).toHaveBeenCalledTimes(1);
      expect(dialogSpy).toHaveBeenCalledWith(ModalViewComponent, emptyTestParams);
    });

    it('should create 1 button', () => {
      expect(spyDialogRef.componentInstance.buttons.length).toEqual(1);
    });
  });

  describe('confirmDialog()', () => {
    let dialogSpy;

    beforeEach(() => {
      dialogSpy = spyOn(ngbModal, 'open').and.returnValue(spyDialogRef);
      service.confirmDialog(testTitle, testTitle, testCallback);
    });

    it('should open modal', () => {
      expect(dialogSpy).toHaveBeenCalledTimes(1);
      expect(dialogSpy).toHaveBeenCalledWith(ModalViewComponent, emptyTestParams);
    });

    it('should create 2 buttons', () => {
      expect(spyDialogRef.componentInstance.buttons.length).toEqual(2);
    });
  });

  describe('deleteConfirmDialog()', () => {
    let dialogSpy;

    beforeEach(() => {
      dialogSpy = spyOn(ngbModal, 'open').and.returnValue(spyDialogRef);
      service.deleteConfirmDialog(testTitle, testCallback);
    });

    it('should open modal', () => {
      expect(dialogSpy).toHaveBeenCalledTimes(1);
      expect(dialogSpy).toHaveBeenCalledWith(ModalViewComponent, deleteTestParams);
    });

    it('should create 2 buttons', () => {
      expect(spyDialogRef.componentInstance.buttons.length).toEqual(2);
    });
  });

  describe('editorDialog()', () => {
    let dialogSpy;

    beforeEach(() => {
      dialogSpy = spyOn(ngbModal, 'open').and.returnValue(spyDialogRef);
      service.editorDialog(testTitle, testTitle, testCallback);
    });

    it('should open modal', () => {
      expect(dialogSpy).toHaveBeenCalledTimes(1);
      expect(dialogSpy).toHaveBeenCalledWith(ModalViewComponent, testParams);
    });

    it('should create 2 buttons', () => {
      expect(spyDialogRef.componentInstance.buttons.length).toEqual(2);
    });
  });
});
