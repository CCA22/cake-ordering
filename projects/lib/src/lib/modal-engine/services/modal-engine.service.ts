import { Injectable, TemplateRef } from '@angular/core';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalViewComponent } from '../components/modal-view/modal-view.component';
import { ModalButton } from '../models/modal-button.model';


/**
 * Modal engine utilities
 */
@Injectable({
  providedIn: 'root'
})
export class ModalEngineService {

  constructor(private readonly modalService: NgbModal) { }

  /**
   * Opens dialog popup with the content from {@param content}, with title from {@param content}, with custom buttons from {@param buttons}
   *
   * @param title     Text displayed in the dialog title
   * @param content   Text or template displayed in the dialog body
   * @param buttons   Dialog actions buttons
   * @param handler   Dialog actions handler
   * @param closureHandler   Optional handler for cross button click
   * @param params    Modal dialog parameters
   *
   */
  // tslint:disable-next-line: no-any
  public customDialog(title: string, content: string | TemplateRef<any>, buttons: ModalButton[], buttonHandler: (button: ModalButton, data: string) => void, closureHandler?: () => void, params?: NgbModalOptions): NgbModalRef {
    const modalRef = this.modalService.open(ModalViewComponent, params ?? { });
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.content = content;
    modalRef.componentInstance.buttons = buttons;
    modalRef.componentInstance.actionClick = buttonHandler;
    modalRef.componentInstance.closeClick = closureHandler;
    console.log(modalRef.componentInstance.content);
    return modalRef;
  }

  /**
   * Opens confirmation popup with the content from {@param content}, with title from {@param content}
   *
   * @param title     Text displayed in the dialog title
   * @param content   Text displayed in the dialog body
   * @param buttonHandler   Handler for confirmation event
   * @param closureHandler   Handler for closure event
   *
   */
  public confirmDialog(title: string, content: string, buttonHandler: (button: ModalButton, data: string) => void, closureHandler?: () => void): NgbModalRef {
    const buttons = [
      { buttonClass: 'btn-outline-secondary', buttonText: `No`, id: 'no', data: false },
      { buttonClass: 'btn-primary', buttonText: `Yes`, id: 'yes', data: true }
    ];
    return this.customDialog(title, content, buttons, buttonHandler, closureHandler);
  }

  /**
   * Opens alert popup with the content from {@param content}, with title from {@param content}
   *
   * @param title     Text displayed in the dialog title
   * @param content   Text displayed in the dialog body
   * @param buttonHandler   Dialog actions buttons
   *
   */
  public alertDialog(title: string, content: string, buttonHandler: (button: ModalButton, data: string) => void, closureHandler?: () => void): NgbModalRef {
    const buttons = [{ buttonClass: 'btn-primary', buttonIcon: 'check', buttonText: `OK`, id: 'confirm' }];
    return this.customDialog(title, content, buttons, buttonHandler, closureHandler);
  }

  /**
   * Convenience function to display a standard delete confirmation dialog
   *
   * @param item    Item descriptive text to display to user
   * @param buttonHandler Button callback handler
   */
  public deleteConfirmDialog(item: string, buttonHandler: (button: ModalButton, data: string) => void, closureHandler?: () => void): NgbModalRef {
    const buttons = [
      { buttonClass: 'btn-outline-secondary', buttonText: `Cancel`, id: 'cancel', data: false },
      { buttonClass: 'btn-danger', buttonIcon: 'trash-alt', buttonText: `Delete`, id: 'delete', data: true }
    ];
    const params: NgbModalOptions = { backdrop: 'static' };
    return this.customDialog(`Are you sure?`, `<strong>${item}</strong> will be deleted`, buttons, buttonHandler, closureHandler, params);
  }

  /**
   * Convenience function to display a standard large editor dialog
   *
   * @param title     Text displayed in the dialog title
   * @param content   Text displayed in the dialog body
   * @param buttonHandler Button callback handler
   * @param closureHandler Button callback handler
   */
  // tslint:disable-next-line: no-any
  public editorDialog(title: string, content: string | TemplateRef<any>, buttonHandler: (button: ModalButton, data: string) => void, closureHandler?: () => void): NgbModalRef {
    const buttons = [
      { buttonClass: 'btn-outline-secondary', buttonText: `Cancel`, id: 'cancel', data: false },
      { buttonClass: 'btn-success', buttonIcon: 'check', buttonText: `Save`, id: 'save', data: true, autoClose: false }
    ];
    const params: NgbModalOptions = { size: 'lg', backdrop: 'static' };
    return this.customDialog(title, content, buttons, buttonHandler, closureHandler, params);
  }

}
