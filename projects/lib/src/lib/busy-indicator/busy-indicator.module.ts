import { NgModule } from '@angular/core';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BusyIndicatorComponent } from './busy-indicator.component';


/**
 * A simple busy indicator
 */
@NgModule({
  declarations: [
    BusyIndicatorComponent,
  ],
  imports: [
    FontAwesomeModule,
  ],
  exports: [
    BusyIndicatorComponent,
  ]
})
export class BusyIndicatorModule { }
