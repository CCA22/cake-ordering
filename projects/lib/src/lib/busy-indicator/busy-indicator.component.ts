import { Component, Input } from '@angular/core';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

/**
 * Simple busy spinner
 */
@Component({
  selector: 'lib-busy-indicator',
  templateUrl: './busy-indicator.component.html',
  styleUrls: ['./busy-indicator.component.scss']
})
export class BusyIndicatorComponent {

  /**
   * Font Awesome size value
   */
  @Input() public size: string;

  // expose the font-awesome icons to the template
  // tslint:disable-next-line: completed-docs
  public faSpinner = faSpinner;

}
