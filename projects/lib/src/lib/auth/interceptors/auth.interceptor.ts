import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

/**
 * Interceptor to handle authentication
 */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  /** {@inheritdoc} */
  // tslint:disable-next-line: no-any
  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        Authorization: `Basic Y2tfYzA4OTc5OTFhNjg5YjI5NWVjMWE1MmQ1NjM5OGNlNzU5MzdkZWE5ZDpjc18zYTU5MjgxZTFiMjNjMzJkODY3MTFkYjEyYTU1NzlkOGU1MmI4NWUy`,
      },
    });
    return next.handle(request);
  }
}
