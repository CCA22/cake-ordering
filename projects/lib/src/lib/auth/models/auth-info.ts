/**
 * API authorization model
 */
export interface AuthInfo {
    /**
     * Access expiration as a Date object
     */
    access_expiration_date: Date;

    /**
     * Access token
     */
    access_token: string;

    /**
     * Access token expiration in seconds
     */
    expires_in: number;

    /**
     * Refresh expiration as a Date object
     */
    refresh_expiration_date: Date;

    /**
     * Refresh token
     */
    refresh_token: string;

    /**
     * Refresh token expiration in seconds
     */
    refresh_token_expires_in: number;

    /**
     * Auth token type
     */
    token_type: string;
}
