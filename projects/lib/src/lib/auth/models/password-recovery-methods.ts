/**
 * Password recovery methods
 */
export enum PasswordRecoveryMethods {

  /** Code to email method */
  Email = 'Email',

  /** Code to phone method */
  Sms = 'Sms',

}
