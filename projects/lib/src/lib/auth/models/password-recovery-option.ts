import { PasswordRecoveryMethods } from './password-recovery-methods';

/**
 * Password recovery method model
 */
export interface PasswordRecoveryOption {
  /**
   * Method type
   */
  Method: PasswordRecoveryMethods;

  /**
   * Method GUID
   */
  Id: string;

  /**
   * Method text for display
   */
  Text: string;

}
