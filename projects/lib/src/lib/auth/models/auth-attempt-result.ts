/**
 * Authorization result
 */
export interface AuthAttemptResult {
    /**
     * HTTP status code
     */
    status: number;

    /**
     * true if successful; false otherwise
     */
    success: boolean;

}
