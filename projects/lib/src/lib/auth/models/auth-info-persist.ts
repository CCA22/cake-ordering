import { AuthInfo } from './auth-info';


/**
 * Persisted authorization details
 */
export interface AuthInfoPersist extends AuthInfo {
    /**
     * Access token expiration as utc string
     */
    accessExpirationUtc: string;

    /**
     * Refresh token expiration as utc string
     */
    refreshExpirationUtc: string;
}
