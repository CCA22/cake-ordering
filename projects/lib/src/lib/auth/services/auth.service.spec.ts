import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AuthService, PERSISTENCE_KEY } from './auth.service';
import { ActivatedRoute } from '@angular/router';
import { AuthInfoPersist } from '../models/auth-info-persist';
import { PersistenceScope, PersistenceService } from '../../shared/services/persistence.service';
import { StringService } from '../../shared/services/string.service';

describe('AuthService', () => {
  let service: AuthService;
  const now = new Date();
  const accessExpiration = 43200;
  const refreshExpiration = 86400;
  const accessExpirationDate = () => { const date = new Date(now); date.setSeconds(now.getSeconds() + accessExpiration); return date; };
  const refreshExpirationDate = () => { const date = new Date(now); date.setSeconds(now.getSeconds() + refreshExpiration); return date; };
  const testRefreshToken = '1234567890';
  const testAccessToken = '0987654321';
  const testUsername = 'user';
  const testPassword = 'password';
  const urlRefreshProvider = { provide: ActivatedRoute, useValue: { snapshot: { fragment: null } } };
  const testAuthObject: AuthInfoPersist = {
    access_token: testAccessToken,
    expires_in: accessExpiration,
    refresh_token: testRefreshToken,
    refresh_token_expires_in: refreshExpiration,
    token_type: '',
    access_expiration_date: accessExpirationDate(),
    refresh_expiration_date: refreshExpirationDate(),
    accessExpirationUtc: accessExpirationDate().toUTCString(),
    refreshExpirationUtc: refreshExpirationDate().toUTCString()
  };

  function configureEnvironment(useLocal: boolean, useUrl: boolean, accessExpired: boolean = false, refreshExpired: boolean = false) {
    let authInfo = testAuthObject;
    if (refreshExpired || accessExpired) {
      authInfo = JSON.parse(JSON.stringify(testAuthObject));
      authInfo.access_expiration_date = new Date(authInfo.access_expiration_date);
      authInfo.refresh_expiration_date = new Date(authInfo.refresh_expiration_date);

      if (refreshExpired) {
        authInfo.refresh_expiration_date.setDate(-100);
        authInfo.refreshExpirationUtc = authInfo.refresh_expiration_date.toUTCString();
      }

      if (accessExpired) {
        authInfo.access_expiration_date.setDate(-100);
        authInfo.accessExpirationUtc = authInfo.access_expiration_date.toUTCString();
      }
    }

    // if use local, set test auth into local storage
    if (useLocal) {
      window.localStorage.setItem(PERSISTENCE_KEY, JSON.stringify(authInfo));
    }

    // if use url, update provider
    if (useUrl) {
      urlRefreshProvider.useValue.snapshot.fragment = testRefreshToken;
    }
  }

  function resetEnvironment() {
    window.localStorage.removeItem(PERSISTENCE_KEY);
    urlRefreshProvider.useValue.snapshot.fragment = null;
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        urlRefreshProvider
      ]
    });

    resetEnvironment();
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('apiEndpoint', () => {
    it('should not be empty', () => {
      expect(service.apiEndpoint).toBeTruthy();
    });
  });

  describe('accessToken', () => {
    it('should return access token when valid', (done) => {
      configureEnvironment(true, false, false, false);
      service.initialize().subscribe(result => {
        expect(result.success).toBeTrue();
        expect(service.accessToken).toBeTruthy();
        done();
      });
    });

    it('should return null when invalid', (done) => {
      configureEnvironment(false, false);
      service.initialize().subscribe(result => {
        expect(result.success).toBeFalse();
        expect(service.accessToken).toBeFalsy();
        done();
      });
    });
  });

  describe('createRedirectUrl()', () => {
    it('should append token to url', (done) => {
      configureEnvironment(true, false);
      service.initialize().subscribe(result => {
        expect(result.success).toBeTrue();
        expect(service.createRedirectUrl('')).toEqual(`#${testRefreshToken}`);
        done();
      });
    });
  });

  describe('initialize()', () => {
    it('should return success: false (no url, no persistence)', (done) => {
      service.initialize().subscribe(result => {
        expect(result.success).toBeFalse();
        done();
      });
    });

    it('should return success: true (no url, with persistance, token valid)', (done) => {
      configureEnvironment(true, false);
      service.initialize().subscribe(result => {
        expect(result.success).toBeTrue();
        done();
      });
    });

    it('should return success: false (no url, with persistence, access expired, refresh expired)', (done) => {
      configureEnvironment(true, false, true, true);
      service.initialize().subscribe(result => {
        expect(result.success).toBeFalse();
        done();
      });
    });

    it('should return success: true (no url, with persistence, access expired, refesh valid)', (done) => {
      configureEnvironment(true, false, true, false);
      service.initialize().subscribe(result => {
        expect(result.success).toBeTrue();
        done();
      });
      const httpMock = TestBed.inject(HttpTestingController);
      const request = httpMock.expectOne({ method: 'POST', url: '/api/v1/auth'});
      request.flush(testAuthObject);
    });

    it('should return success: true (with url, no persistence)', inject([HttpTestingController], (httpMock: HttpTestingController) => {
      configureEnvironment(false, true, false, false);
      service.initialize().subscribe(result => {
        expect(result.success).toBeTrue();
      });

      const testRequest = httpMock.expectOne('/api/v1/auth');
      expect(testRequest.request.method).toEqual('POST');
      testRequest.flush(testAuthObject);
    }));
  });

  describe('login', () => {
    it('should return success: true with valid credentials', inject([HttpTestingController], (httpMock: HttpTestingController) => {
      service.login(testUsername, testPassword).subscribe(result => {
        expect(result.success).toBeTrue();
      });

      const testRequest = httpMock.expectOne('/api/v1/auth');
      expect(testRequest.request.method).toEqual('POST');
      testRequest.flush(testAuthObject);
    }));

    it('should return success: false with invalid credentials', inject([HttpTestingController], (httpMock: HttpTestingController) => {
      service.login(testUsername, testPassword).subscribe(result => {
        expect(result.success).toBeFalse();
      });

      const testRequest = httpMock.expectOne('/api/v1/auth');
      expect(testRequest.request.method).toEqual('POST');
      testRequest.flush({}, { status: 401, statusText: 'Unauthorized' });
    }));
  });

  describe('forgot()', () => {
    it('should call API endpoint', inject([HttpTestingController], (httpMock: HttpTestingController) => {
      service.forgot('test@localhost.localdomain').subscribe(result => {
        expect(result.length).toBeTruthy();
      });

      const req = httpMock.expectOne({ method: 'GET', url: '/api/v1/auth/forgotpassword?username=test@localhost.localdomain' });
      req.flush([{}, {}]);
    }));
  });

  describe('code()', () => {
    it('should call API endpoint', inject([HttpTestingController], (httpMock: HttpTestingController) => {
      service.code('test@localhost.localdomain', '1').subscribe(result => {
        expect(result).toBeTruthy();
      });

      const req = httpMock.expectOne({ method: 'POST', url: '/api/v1/auth/forgotpassword?username=test@localhost.localdomain&methodId=1' });
      req.flush('test');
    }));
  });

  describe('logout', () => {
    it('should clear stored auth information', (done) => {
      configureEnvironment(true, false, false, false);
      service.initialize().subscribe(result => {
        expect(result.success).toBeTrue();
        service.logout();
        expect(service.accessToken).toBeFalsy();
        done();
      });
    });
  });

});
