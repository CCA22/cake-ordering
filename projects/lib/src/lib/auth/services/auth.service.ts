import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { PersistenceScope, PersistenceService } from '../../shared/services/persistence.service';
import { StringService } from '../../shared/services/string.service';
import { UrlService } from '../../shared/services/url.service';
import { AuthAttemptResult } from '../models/auth-attempt-result';
import { AuthInfo } from '../models/auth-info';
import { AuthInfoPersist } from '../models/auth-info-persist';
import { PasswordRecoveryOption } from '../models/password-recovery-option';

export const PERSISTENCE_KEY = 'speakcoreAuth';
export const PERSISTENCE_SCOPE = PersistenceScope.Device;
const REFRESH_INTERVAL = 30000;

/**
 * User authenication utilities
 */
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /**
   * Api endpoint path for authorization/authentication
   */
  public readonly apiEndpoint = '/auth';

  private authInfo: AuthInfo;
  private refreshIntervalId: number;
  private refreshTimerId: number;

  /**
   * Gets the current access token.
   * @returns the current valid access token, null if it doesn't exist or if it is invalid/expired
   */
  public get accessToken(): string {
    let accessToken = this.authInfo?.access_token;
    if (this.isAccessExpired(this.authInfo)) {
      accessToken = null;
    }

    return accessToken;
  }

  /**
   * Get authorization header
   */
  public get authHeaders(): { [name: string]: string | string[] } {
    return { Authorization: `Bearer ${this.accessToken}` };
  }

  constructor(
    private readonly httpClient: HttpClient,
    private readonly activatedRoute: ActivatedRoute,
    private readonly persistenceService: PersistenceService,
    private readonly stringService: StringService,
    private readonly urlService: UrlService) { }

  /**
   * Appends the base-64 refresh token to a url
   * @param url url to append the token to
   * @returns url containing the base 64 version of the existing refresh token appended as a hash
   */
  public createRedirectUrl(url: string): string {
    let authUrl = url;
    if (this.authInfo?.refresh_token) {
      authUrl = `${url}#${this.authInfo.refresh_token}`;
    }

    return authUrl;
  }

  /**
   * Initializes the auth service.
   * @returns observable containing the initialization result
   */
  public initialize(): Observable<AuthAttemptResult> {
    // if we get a refresh token from the url fragment, return the refresh request for it
    if (this.activatedRoute?.snapshot?.fragment) {
      const refreshToken = this.stringService.fromBase64(this.activatedRoute.snapshot.fragment);
      if (refreshToken) {
        return this.requestRefresh(refreshToken);
      }
    }

    // if we get here we don't have a valid token
    // attempt to load persisted authinfo and map to authattemptresult
    return this.loadAuthInfo().pipe(
      mergeMap(authInfoPersist => {
        let response = of({ success: false, status: null }); // default failure response
        if (authInfoPersist) {
          if (!this.isAccessExpired(authInfoPersist)) {
            // auth token is not expired:
            // set local object, start refresh timer, set success reponse
            this.authInfo = authInfoPersist;
            this.startRefreshTimerByDate(this.authInfo.access_expiration_date);
            response = of({ success: true, status: null });
          } else if (!this.isRefreshExpired(authInfoPersist)) {
            // auth token is expired but refresh token is not expired:
            // set refresh request as response
            response = this.requestRefresh(authInfoPersist.refresh_token);
          }
        }

        // return whatever response we have
        return response;
      })
    );
  }

  /**
   * Attempts to log the user in.
   * @param username username of the user
   * @param password password of the user
   * @returns observable containing the login result
   */
  public login(username: string, password: string): Observable<AuthAttemptResult> {
    let result = of({
      success: false,
      status: null
    });

    if (username) {
      const formData = this.getLoginFormData(username, password);
      result = this.sendAuthRequest(formData);
    }

    return result;
  }

  /**
   * Password recovery methods request
   * @param username username for password recovery
   * @returns observable containing password recovery methods
   */
  public forgot(username: string): Observable<PasswordRecoveryOption[]> {
    const url = this.urlService.getApiUrl(1, `${this.apiEndpoint}/forgotpassword`);
    return this.httpClient.get<PasswordRecoveryOption[]>(url, { params: { username } });
  }

  /**
   * Password reset code request
   * @param username username for password recovery
   * @param methodId recovery method ID
   * @returns observable containing password recovery methods
   */
  public code(username: string, methodId: string): Observable<void> {
    const url = this.urlService.getApiUrl(1, `${this.apiEndpoint}/forgotpassword`);
    // tslint:disable-next-line: invalid-void
    return this.httpClient.post<void>(url, null, { params: { username, methodId } });
  }

  /**
   * Logs the user out
   */
  public logout(): void {
    this.stopRefreshRecoveryInterval();
    this.persistenceService.remove(PERSISTENCE_SCOPE, PERSISTENCE_KEY);
    this.authInfo = null;
  }

  /**
   * Gets an expiration date based on a start date and number of seconds.
   * @param startDate The date to calculate epxiration from
   * @param seconds The number of seconds until expiration
   * @returns date object corresponding to the provided parameters
   */
  private getExpirationDate(startDate: Date, seconds: number): Date {
    const expirationDate = new Date(startDate);
    expirationDate.setSeconds(startDate.getSeconds() + seconds);

    return expirationDate;
  }

  /**
   * Get form data for login request
   * @param username username of user
   * @param password password of user
   * @returns form data string
   */
  private getLoginFormData(username: string, password: string): string {
    return `username=${username}&password=${password}&grant_type=password`;
  }

  /**
   * Get form data for refresh request
   * @param refreshToken refresh token
   * @returns form data string
   */
  private getRefreshFormData(refreshToken: string): string {
    return `refresh_token=${refreshToken}&grant_type=refresh_token`;
  }

  /**
   * Determines if an auth object's access token is expired
   * @param authInfo auth object containing the access token
   * @returns true if access token exists and is not expired, otherwise false
   */
  private isAccessExpired(authInfo: AuthInfo): boolean {
    return authInfo?.access_token && authInfo?.access_expiration_date <= new Date();
  }

  /**
   * Determines if an auth object's refresh token is expired
   * @param authInfo The auth object containing the refresh token
   * @returns true if refresh token exists and is not expired, otherwise false
   */
  private isRefreshExpired(authInfo: AuthInfo): boolean {
    return authInfo?.refresh_token && authInfo?.refresh_expiration_date <= new Date();
  }

  /**
   * Loads the locally persisted authentication information, if any
   * @returns locally persisted auth info, if any
   */
  private loadAuthInfo(): Observable<AuthInfoPersist> {
    return this.persistenceService.load<AuthInfoPersist>(PERSISTENCE_SCOPE, PERSISTENCE_KEY)
    .pipe(
      tap(authInfoPersist => {
        if (authInfoPersist) {
          authInfoPersist.access_expiration_date = new Date(authInfoPersist.accessExpirationUtc);
          authInfoPersist.refresh_expiration_date = new Date(authInfoPersist.refreshExpirationUtc);
        }
      })
    );
  }

  /**
   * Sends a refresh request
   * @param refreshToken The refresh token for the request
   * @returns observable containing authentication result
   */
  private requestRefresh(refreshToken: string): Observable<AuthAttemptResult> {
    const formData = this.getRefreshFormData(refreshToken);
    return this.sendAuthRequest(formData);
  }

  /**
   * Stores authentication information locally
   * @param authInfo The authentication object to store
   */
  private saveAuthInfo(authInfo: AuthInfo): void {
    if (authInfo) {
      const authInfoPersist: AuthInfoPersist = {
        token_type: authInfo.token_type,
        access_token: authInfo.access_token,
        expires_in: authInfo.expires_in,
        refresh_token: authInfo.refresh_token,
        refresh_token_expires_in: authInfo.refresh_token_expires_in,
        access_expiration_date: authInfo.access_expiration_date,
        refresh_expiration_date: authInfo.refresh_expiration_date,
        accessExpirationUtc: authInfo.access_expiration_date.toUTCString(),
        refreshExpirationUtc: authInfo.refresh_expiration_date.toUTCString()
      };

      this.persistenceService.save(PERSISTENCE_SCOPE, PERSISTENCE_KEY, authInfoPersist);
    }
  }

  /**
   * Sends an authentication request to the server
   * @returns observable containing authentication result
   */
  private sendAuthRequest(formData: string): Observable<AuthAttemptResult> {
    // tslint:disable-next-line: completed-docs no-any
    const options: { headers: HttpHeaders; observe: any; } = {
      observe: 'response',
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };

    const url = this.urlService.getApiUrl(1, this.apiEndpoint);
    return this.httpClient.post<AuthInfo>(url, formData, options).pipe(
      map((response: HttpResponse<AuthInfo>) => {
        const result: AuthAttemptResult = {
          success: false,
          status: null
        };

        if (response) {
          result.status = response.status;
        }

        this.authInfo = response.body;
        result.success = !!this.authInfo;

        const now = new Date();
        this.authInfo.access_expiration_date = this.getExpirationDate(now, this.authInfo.expires_in);
        this.authInfo.refresh_expiration_date = this.getExpirationDate(now, this.authInfo.refresh_token_expires_in);

        this.saveAuthInfo(this.authInfo);

        if (result.success) {
          this.startRefreshTimer(this.authInfo.expires_in * 1000);
        }

        return result;
      }),
      catchError((error: HttpErrorResponse) => {
        const result: AuthAttemptResult = {
          status: error.status,
          success: false
        };

        return of(result);
      })
    );
  }

  /**
   * Starts the refresh interval timer
   * Attempts auth refresh on interval until cancelled
   */
  private startRefreshRecoveryInterval(interval: number): void {
    if (this.refreshIntervalId) {
      window.clearInterval(this.refreshIntervalId);
    }

    this.refreshIntervalId = window.setInterval(() => {
      if (this.isRefreshExpired(this.authInfo)) {
        // clear auth info
        this.stopRefreshRecoveryInterval();
      } else {
        this.requestRefresh(this.authInfo.refresh_token)
          .subscribe(authResult => {
            if (authResult && authResult.success) {
              this.stopRefreshRecoveryInterval();
            }
          });
      }
    }, interval);
  }

  /**
   * Starts the refresh timer
   * @param timeout The timeout for the timer, should coincide with the expiration of the access token.
   */
  private startRefreshTimer(timeout: number): void {
    if (this.refreshTimerId) {
      window.clearTimeout(this.refreshTimerId);
    }

    this.refreshTimerId = window.setTimeout(() => {
      this.refreshTimerId = null;

      if (!this.isRefreshExpired(this.authInfo)) {
        this.requestRefresh(this.authInfo.refresh_token)
          .subscribe(authResult => {
            if (!authResult || !authResult.success) {
              this.startRefreshRecoveryInterval(REFRESH_INTERVAL);
            }
          });
      }
    }, timeout);
  }

  /**
   * Starts the refresh timer using a provided expiration date
   * @param expirationDate the expiration date for the timeout, used to calculate timeout from 'now'
   */
  private startRefreshTimerByDate(expirationDate: Date): void {
    const now = new Date();
    const timeout = expirationDate.getTime() - now.getTime();
    this.startRefreshTimer(timeout);
  }

  /**
   * Stops the current refresh interval, if any
   */
  private stopRefreshRecoveryInterval(): void {
    if (this.refreshIntervalId) {
      window.clearInterval(this.refreshIntervalId);
      this.refreshIntervalId = null;
    }
  }
}
