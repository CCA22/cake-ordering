import { NgModule } from '@angular/core';
import { FaConfig, FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';


/**
 * Font Awesome
 */
@NgModule({
  declarations: [],
  imports: [
    FontAwesomeModule,
  ],
  exports: [
    FontAwesomeModule,
  ],
})
export class IconsModule {
  constructor(faLibrary: FaIconLibrary, faConfig: FaConfig) {
    faLibrary.addIconPacks(fas);
    faConfig.defaultPrefix = 'fas';
    faConfig.fixedWidth = true;
  }
}
