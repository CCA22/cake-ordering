/**
 * Represents a user
 */
export interface User {
  /** First Name */
  First: string;
  /** Last Name */
  Last: string;
  /** Email Address */
  EmailAddress: string;
  /** Phone Number */
  Phone: string;
}
