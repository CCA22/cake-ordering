/**
 * Bootstrap contextual color values
 */
export enum ContextualColor {
  // tslint:disable: completed-docs
  Primary = 'primary',
  Secondary = 'secondary',
  Success = 'success',
  Danger = 'danger',
  Warning = 'warning',
  Info = 'info',
  Light = 'light',
  Dark = 'dark',
  Muted = 'muted',
  White = 'white',
}
