import { Injectable } from '@angular/core';
import { CanActivate, UrlTree } from '@angular/router';
import { Observable, ReplaySubject } from 'rxjs';

/** Location Selector Guard */
@Injectable({
  providedIn: 'root'
})
export class LocationSelectorGuard implements CanActivate {

  constructor(
    ) { }

  /** {@inheritdoc } */
  public canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const result = new ReplaySubject<boolean>(1);
    const isSet = false;
    if (isSet) {
      return true;
    } else {

    }
    return result.asObservable();
  }
}

