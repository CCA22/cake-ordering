import { TestBed } from '@angular/core/testing';

import { PersistenceService, PersistenceScope } from './persistence.service';

describe('PersistenceService', () => {
  let service: PersistenceService;
  const testKey = 'testKey';
  const testValue = 'testValue';

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PersistenceService);
  });

  afterEach(() => {
    window.localStorage.clear();
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('save()/load()', () => {
    it('should round-trip encode/decode object', (done) => {
      const value = {
        name: 'test',
        items: [ 1, 2, 3 ]
      };
      service.save(PersistenceScope.Device, testKey, value);
      service.load(PersistenceScope.Device, testKey).subscribe(result => {
        expect(result).toEqual(value);
        done();
      })
    });
  });
});
