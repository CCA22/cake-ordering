import { Injectable } from '@angular/core';
import { LocationService } from './location.service';


/**
 * URL utilities
 */
@Injectable({
  providedIn: 'root'
})
export class UrlService {

  /**
   * Authentication error query string key
   * Used by login to determine if the user was sent via an authentication error
   */
  public readonly authErrorKey = 'authError';

  /**
   * Login route path
   */
  public readonly loginPath = '/login';

  /**
   * Return url query string key
   * Used by login to determine where to forward the user to, if populated.
   */
  public readonly returnUrlKey = 'returnUrl';


  private readonly apiUrl = '/wp-json/wc';
  private readonly apiVPath = '/v';
  private readonly validVersions = [ 3 ];

  constructor(private readonly locationService: LocationService) { }

  /**
   * Get a versioned api url
   *
   * @param version version number
   * @param path    URL path
   *
   * @returns the base api url based on the provided version number, defaults to version 1
   */
  public getApiUrl(version: number, path: string): string {
    if (this.validVersions.every(validVersion => validVersion !== version)) {
      console.error(`invalid API version ${version}`);
      version = this.validVersions[0];
    }
    return `https://staging.thehappymixer.com${this.apiUrl}${this.apiVPath}${version}${path}`;
  }

  /**
   * Redirects the client to the login page
   *
   * @param error           Error code (optional; default 0)
   * @param withReturnUrl   Include a return URL (optional; default true)
   */
  public redirectToLogin(error: number = 0, withReturnUrl: boolean = true): void {
    const returnUrl = location.pathname;
    if (returnUrl !== this.loginPath) {
      let urlPath = `${this.loginPath}?${this.authErrorKey}=${error}`;
      if (withReturnUrl) {
        urlPath += `&${this.returnUrlKey}=${encodeURIComponent(`${returnUrl}${location.search}`)}`;
      }
      this.locationService.href = urlPath;
    }
  }

}
