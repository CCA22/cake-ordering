import { TestBed } from '@angular/core/testing';

import { UrlService } from './url.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('UrlService', () => {
  let service: UrlService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(UrlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('authErrorKey', () => {
    it('should not be empty', () => {
      expect(service.authErrorKey).toBeTruthy();
    });
  });

  describe('loginPath', () => {
    it('should not be empty', () => {
      expect(service.loginPath).toBeTruthy();
    });
  });

  describe('returnUrlKey', () => {
    it('should not be empty', () => {
      expect(service.returnUrlKey).toBeTruthy();
    });
  });

  describe('getApiUrl()', () => {
    it('should return v3 by default', () => {
      expect(service.getApiUrl(null, '')).toContain('v3');
    });

    it('should return v3', () => {
      expect(service.getApiUrl(1 , '')).toContain('v3');
    });
/* there's currently only 1 version
    it('should return v2', () => {
      expect(service.getApiUrl(2)).toContain('v2');
    });

    it('should return v3', () => {
      expect(service.getApiUrl(3)).toContain('v3');
    });
*/
  });
});
