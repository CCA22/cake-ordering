import { Injectable } from '@angular/core';


/**
 * Browser location utilities
 */
@Injectable({
  providedIn: 'root'
})
export class LocationService {

  /**
   * Get the current location
   *
   * @returns URL string
   */
  public get href(): string {
    return window.location.href;
  }

  /**
   * Set the current location URL
   *
   * @param value URL string
   */
  public set href(value: string) {
    window.location.href = value;
  }

}
