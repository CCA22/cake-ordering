import { Injectable } from '@angular/core';


/**
 * String utilities
 */
@Injectable({
  providedIn: 'root'
})
export class StringService {

  /**
   * Decodes a base 64 encoded string
   * @param s the string to decode
   * @returns decoded string
   */
  public fromBase64(s: string): string {
    let result: string = null;
    if (s) {
      result = atob(s);
    }

    return result;
  }

  /**
   * Encodes a string in base 64
   * @param s the string to encode
   * @returns encoded base 64 string
   */
  public toBase64(s: string): string {
    let result = '';
    if (s) {
      result = btoa(s);
    }

    return result;
  }

  /**
   * Parses a string into a typed object
   * @param jsonString json string to parse
   * @returns object parsed from json
   */
  public fromJson<T>(jsonString: string): T {
    let result: T = null;
    if (jsonString) {
      result = JSON.parse(jsonString) as T;
    }

    return result;
  }

  /**
   * Stringifies an object into json
   * @param obj the object to stringify
   * @returns json string
   */
  public toJson<T>(obj: T): string {
    return JSON.stringify(obj);
  }

  /**
   *  Return this string concatenated with the specified {@see value} string, delimited by the given {@see delimiter} if this string is not empty
   *
   * @param base                String to append to
   * @param value               String or array of strings to append
   * @param delimiter           Delimiter to use if base string is not empty
   * @param includeEmptyValue   Include empty delimited values (optional; default true)
   *
   * @returns concatenated string
   */
  public appendDelimited(base: string, value: string | string[], delimiter: string, includeEmptyValue: boolean = true): string {
    if (Array.isArray(value)) {
      return value.reduce((prev, curr) => this.appendDelimited(prev, curr, delimiter, includeEmptyValue), base);
    }
    return (base ?? '') +
           ((!!base && (includeEmptyValue || !!value)) ? delimiter : '') +
           ((value as string) ?? '');
  }

}
