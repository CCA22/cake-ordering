import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { StringService } from './string.service';


/**
 * Scope of persisted data
 */
export enum PersistenceScope {

  /** On this device only */
  Device,

  /** On this device and other devices of a similar UI class */
  Class,

  /** For this user on any device */
  User,
}


/**
 * Persistent storage utilities
 */
@Injectable({
  providedIn: 'root'
})
export class PersistenceService {

  constructor(private readonly stringService: StringService) { }

  /**
   * Store a value in peristent storage
   *
   * @param scope Persistence scope
   * @param key Unique identifier
   * @param value Value to store
   */
  public save<T>(scope: PersistenceScope, key: string, value: T): void {
    const encoded = this.stringService.toJson(value);
    switch (scope) {
      case PersistenceScope.Class:  // TODO
      case PersistenceScope.User:   // TODO
      case PersistenceScope.Device:
        this.saveLocalStorage(key, encoded);
        break;
    }
  }

  /**
   * Retreive a value from peristent storage
   *
   * @param scope Persistence scope
   * @param key Unique identifier
   *
   * @returns Observable of T or NULL if not found
   */
  public load<T>(scope: PersistenceScope, key: string): Observable<T> {
    switch (scope) {
      case PersistenceScope.Class:  // TODO
      case PersistenceScope.User:   // TODO
      case PersistenceScope.Device: {
        const encoded = this.loadLocalStorage(key);
        return of(this.stringService.fromJson<T>(encoded));
      }
    }
  }

  /**
   * Remove a value from persistent storage
   * @param scope Persistence scope
   * @param key Unique identifier
   */
  public remove(scope: PersistenceScope, key: string): void {
    switch (scope) {
      case PersistenceScope.Class:  // TODO
      case PersistenceScope.User:   // TODO
      case PersistenceScope.Device:
        this.removeLocalStorage(key);
        break;
    }
  }

  /**
   * Saves a string value to local storage
   * @param key key of the value to load
   * @param value string value to store
   */
  private saveLocalStorage(key: string, value: string): void {
    if (key) {
      window.localStorage.setItem(key, value);
    }
  }

  /**
   * Loads a string value from local storage
   * @param key key of the value to load
   * @returns stored string value
   */
  private loadLocalStorage(key: string): string {
    return window.localStorage.getItem(key);
  }

  /**
   * Removes a value from local storage
   * @param key key of the value to remove
   */
  private removeLocalStorage(key: string): void {
    if (key) {
      window.localStorage.removeItem(key);
    }
  }

}
