import { TestBed } from '@angular/core/testing';
import { StringService } from './string.service';


describe('StringService', () => {
  let service: StringService;

  beforeEach(() => {
    TestBed.configureTestingModule({
    });
    service = TestBed.inject(StringService);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('fromBase64()', () => {
    it('should decode string from base 64', () => {
      expect(service.fromBase64('dGVzdA==')).toEqual('test');
    });
  });

  describe('toBase64()', () => {
    it('should encode string to base 64', () => {
      expect(service.toBase64('test')).toEqual('dGVzdA==');
    });
  });

  describe('toJson()', () => {
    it('should stringify object to json', () => {
      expect(service.toJson({ id: 1 })).toEqual('{"id":1}');
    });
  });

  describe('fromJson()', () => {
    it('should parse json string to object', () => {
      const obj: any = service.fromJson('{"id":1}');
      expect(obj.id).toEqual(1);
    });
  });

  describe('appendDelimited()', () => {

    it('should return value when base is empty', () => {
      const value = service.appendDelimited('', 'value', ',');
      expect(value).toBe('value');
    });

    it('should return value when base is null', () => {
      const value = service.appendDelimited(null, 'value', ',');
      expect(value).toBe('value');
    });

    it('should return concatenated value', () => {
      const value = service.appendDelimited('base', 'value', ',');
      expect(value).toBe('base,value');
    });

    it('should return base when value is empty and includeEmptyValue is false', () => {
      const value = service.appendDelimited('base', '', ',', false);
      expect(value).toBe('base');
    });

    it('should return base when value is null and includeEmptyValue is false', () => {
      const value = service.appendDelimited('base', null, ',', false);
      expect(value).toBe('base');
    });

    it('should return base and delimiter when value is empty and includeEmptyValue is true', () => {
      const value = service.appendDelimited('base', '', ',', true);
      expect(value).toBe('base,');
    });

    it('should return base when value is null and includeEmptyValue is true', () => {
      const value = service.appendDelimited('base', null, ',', true);
      expect(value).toBe('base,');
    });

  });

});
