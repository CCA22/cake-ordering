import { HttpClient, HttpResponse } from '@angular/common/http';
import { Optional } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Guid } from '../models/guid.type';
import { UrlService } from './url.service';


/**
 *  General purpose data service that provides standard root functions for {@see Base} models
 *
 * @param Tlist   Outbound list item API model
 * @param Tout    Outbound detailed API model
 * @param Tin     Inbound API model
 */
export abstract class BaseDataService<TList, Tin, Tout> {

  constructor(@Optional() protected readonly basePath: string,
              protected readonly urlService: UrlService,
              protected readonly httpClient: HttpClient) { }

  /**
   * Get the list of {@typedef Tlist} objects
   */
  public getList(): Observable<TList> {
    const url = this.urlService.getApiUrl(3, this.basePath);
    return this.httpClient.get<TList>(url);
  }

  /**
   * Get a specific {@typedef TOut} object by unique identifier
   *
   * @param id    Object unique identifier
   * @returns T observable
   */
  public get(id: Guid): Observable<Tout> {
    const url = this.urlService.getApiUrl(3, `${this.basePath}/${id}`);
    return this.httpClient.get<Tout>(url);
  }


  /**
   * Create a new {@typedef T} item
   *
   * @param value   Complete object
   *
   * @returns New object ID
   */
  public create(value: Tin): Observable<Guid> {
    const url = this.urlService.getApiUrl(1, this.basePath);
    return this.httpClient.post<HttpResponse<void>>(url, value, { observe: 'response' }).pipe(
      map(response => response.headers.get('Location'))
    );
  }

  /**
   * Update a specific {@typedef Tin} by unique identifier
   *
   * @param id      Object unique identifier
   * @param value   Complete object
   *
   * @returns HTTP status code
   */
  public set(id: number, value: Tin): Observable<number> {
    const url = this.urlService.getApiUrl(1, `${this.basePath}/${id}`);
    return this.httpClient.put<HttpResponse<void>>(url, value, { observe: 'response' }).pipe(
      map(response => response.status)
    );
  }

  /**
   * Update a specific {@typedef Tin} by unique identifier
   *
   * @param id      Object unique identifier
   * @param value   Partial object
   *
   * @returns HTTP status code
   */
  public update(id: number, value: object): Observable<number> {
    const url = this.urlService.getApiUrl(1, `${this.basePath}/${id}`);
    return this.httpClient.patch<HttpResponse<void>>(url, value, { observe: 'response' }).pipe(
      map(response => response.status)
    );
  }

  /**
   * Delete a specific {@typedef Tin} by unique identifier
   *
   * @param id      Object unique identifier
   *
   * @returns HTTP status code
   */
  public delete(id: number): Observable<number> {
    const url = this.urlService.getApiUrl(1, `${this.basePath}/${id}`);
    return this.httpClient.delete<HttpResponse<void>>(url, { observe: 'response' }).pipe(
      map(response => response.status)
    );
  }
}
