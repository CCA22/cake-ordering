import { NgModule } from '@angular/core';

/**
 * Common utilities
 */
@NgModule({
  declarations: [
  ],
  exports: [
  ]
})
export class SharedModule { }
