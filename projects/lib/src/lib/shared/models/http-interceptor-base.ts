import { HttpRequest } from '@angular/common/http';
import { HttpRequestType } from './http-request-type';
import { HttpRequestTypeParams } from './http-request-type-params';

/**
 * Base class for interceptors
 */
export class HttpInterceptorBase {
  /**
   * Gets the request type from the http params
   * @param request The type of request
   */
  public getRequestType(request: HttpRequest<unknown>): HttpRequestType {
    let requestType: HttpRequestType = null;
    if (request.params instanceof HttpRequestTypeParams) {
      requestType = request.params.requestType;
    }

    return requestType;
  }
}
