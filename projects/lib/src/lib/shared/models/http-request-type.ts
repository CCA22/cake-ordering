/**
 * Request type for pipeline processing, such as interceptors
 */
export enum HttpRequestType {
  /**
   * The request is for logging an error
   */
  ErrorLog = 1
}
