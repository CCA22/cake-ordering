import { HttpParams } from '@angular/common/http';
import { HttpRequestType } from './http-request-type';

/**
 * Allows passing of a request type in an HttpRequest
 * Used for passing data to interceptors
 */
export class HttpRequestTypeParams extends HttpParams {

  /**
   * The type of request
   */
  public requestType: HttpRequestType;

  constructor(requestType: HttpRequestType) {
      super();
      this.requestType = requestType;
  }
}
