import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppOverlays } from '../app-overlay/enums/app-overlays.enum';
import { AppOverlayService } from '../app-overlay/services/app-overlay.service';
import { BaseDataService } from './services/base-data.service';

/**
 * Generic resolver ensuring retrieval of a specific item
 */
export abstract class BaseDataResolver<Tmodel> implements Resolve<Tmodel> {

  constructor(
    // tslint:disable-next-line: no-any
    private readonly service: BaseDataService<any, Tmodel, any>,
    private readonly overlayService: AppOverlayService) { }

  /** {@inheritdoc} */
  // tslint:disable-next-line: variable-name
  public resolve(route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): Observable<Tmodel> {
    const id = route.paramMap.get('id');
    this.overlayService.showOverlay(AppOverlays.Loading);
    const dataRequest = this.service.get(id) // load existing
    .pipe(
      tap(() => {
        this.overlayService.hideOverlay(AppOverlays.Loading);
      })
    );

    return dataRequest;
  }
}
